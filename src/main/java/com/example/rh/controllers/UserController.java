package com.example.rh.controllers;

import com.example.rh.listeners.UserListener;
import com.example.rh.mqs.UserMQSender;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(path = "/users")
public class UserController {
    private final UserMQSender userSender;

    public UserController(UserMQSender userSender) {
        this.userSender = userSender;
    }

    @GetMapping
    public ResponseEntity<List<String>> getAllAction() {
        return ResponseEntity.ok(Arrays.asList("Blop", "BiBlop", "Plop"));
    }

//    @GetMapping(path = "/poke")
//    public ResponseEntity<List> getAllPokemon() {
//        URI uri = URI.create("https://pokeapi.co/api/v2/ability/?limit=20&offset=20");
////        return ResponseEntity.ok(restTemplate.getForObject(uri, List.class));
//    }

    @GetMapping("/create")
    public ResponseEntity<String> createAction() {

        userSender.send("Hello there!");
        return ResponseEntity.ok("CREATED");
    }
}
