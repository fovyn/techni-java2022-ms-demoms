package com.example.rh.listeners;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserListener {
    @RabbitListener(bindings = {
            @QueueBinding(
                    exchange = @Exchange(name = "user"),
                    value= @Queue(name = "q-user-rh", durable = "true")
            )
    })
    public void newUser(String msg) {
        log.info("New user => {}", msg);
    }
}
